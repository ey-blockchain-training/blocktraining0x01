const config = {
  accounts: [
    '0x627306090abaB3A6e1400e9345bC60c78a8BEf57',
    '0xf17f52151EbEF6C7334FAD080c5704D77216b732',
    '0xC5fdf4076b8F3A5357c5E395ab970B5B54098Fef',
    '0x821aEa9a577a9b44299B9c15c88cf3087F3b5544',
    '0x0d1d4e623D10F9FBA5Db95830F7d3839406C6AF2',
  ],
  mnemonic: 'candy maple cake sugar pudding cream honey rich smooth crumble sweet treat',
  authorAddress: '0xf17f52151EbEF6C7334FAD080c5704D77216b732',
  copyEditorAddress: '0xC5fdf4076b8F3A5357c5E395ab970B5B54098Fef',
  seniorEditorAddress: '0x821aEa9a577a9b44299B9c15c88cf3087F3b5544',
  articleAddress: '0x345ca3e014aaf5dca488057592ee47305d9b3e10',
};

module.exports = config;
