import React, { Component } from 'react';
import queryString from 'query-string';
import TimeAgo from 'react-timeago';
import ReactTable from 'react-table';
import 'react-table/react-table.css';

import {
  Form,
  Input,
  Textarea,
  Select,
} from 'formsy-react-components';

import {
  Label,
  Row,
  Col,
  Panel,
  PanelGroup,
  ListGroup,
  ListGroupItem,
  Button,
} from 'react-bootstrap';

class ArticleEdit extends Component {

  constructor( props ){
    super( props );
    this.handleRoleChange = this.handleRoleChange.bind( this );
    this.submitForm = this.submitForm.bind( this );

    this.state = {
      role: 'author',
      currentRevision: 0,
      revisionCount: 0,
      title: '',
      body: '',
      timestamp: '',
      comment: '',
      revisions: [],
      articleStatus: 'Draft',
    }
  }

  componentWillMount() {
    this.getData();
  }

  getData() {
    let revisionNumber = this.props.match.params.id === 'new' ? null : this.props.match.params.id;
    if( revisionNumber ){
      this.setState({ currentRevision: revisionNumber });
      this.loadRevision( parseInt( revisionNumber, 10 ) )
        .then( currentRevision => this.setState( currentRevision ))
        .then( () => this.loadRevisions() )
        .then( revisions => this.setState({ revisions }))
        .then( () => this.loadArticleStatus() )
        .then( articleStatus => this.setState({ articleStatus }))
    }
  }

  loadRevision( revisionNumber ) {
    return new Promise( resolve => {
      let query = queryString.stringify({ revisionNumber });
      fetch( `http://localhost:3031/getRevisionInfo?${ query }`)
        .then( res => res.json() )
        .then( result => resolve( result ) );
    });
  }

  loadRevisions() {
    return new Promise( resolve => {
      let revisions = [...Array( this.state.revisionCount + 1 )].map( ( _, id ) => {
        return new Promise( resolve => {
          this.loadRevision( id ).then( result => resolve( result ) );
        });
      });
      Promise.all( revisions ).then( results => resolve( results ));
    });
  }

  loadArticleStatus() {
    return new Promise( resolve => {
      fetch( `http://localhost:3031/getArticleStatus`)
        .then( res => res.json() )
        .then( result => resolve( result ) );
    });
  }

  submitForm( formData ){
    // Calls the API to submit a new Article
    let { title, body, comment } = formData;
    let query = queryString.stringify({ title, body, comment });
    let action = this.state.revisionCount === 0 ? 'createarticle' : 'revisearticle';
    fetch( `http://localhost:3031/${ action }?${ query }` ) // Example: localhost:3031/createarticle?title=TitleTest&body=BodyTest
      .then( res => res.json() )
      .then( result => {
        alert(`Article Contract Address: ${ result }`);
        this.props.history.push('/article/0');
        this.getData();
      });
  }

  handleApprove(){
    let action;
    switch( this.state.role ){
      case 'seniorEditor':
        action = 'approveAsSenior';
        break;
      case 'copyEditor':
      default:
        action = 'approveArticle';
        break;
    }

    if( action ){
      fetch( `http://localhost:3031/${ action }`)
        .then( res => res.json() )
        .then( result => {
          console.log( result );
          this.getData();
        });
    }
  }

  handleDecline(){
    let formData = this.refs.form.formsyForm.getModel();
    let { title, body, comment } = formData;

    let action;
    switch( this.state.role ){
      case 'seniorEditor':
        action = 'rejectAsSenior';
        break;
      case 'copyEditor':
      default:
        action = 'rejectArticle';
        break;
    }

    let query = queryString.stringify({ title, body, comment });
    fetch( `http://localhost:3031/${action}?${query}`)
      .then( res => res.json() )
      .then( result => {
        console.log(result);
        this.getData();
      });
  }

  handleRoleChange( role ){
    this.setState({ role });
  }

  isEditor() {
    switch( this.state.role ){
      case 'seniorEditor':
      case 'copyEditor':
        return true;
      case 'author':
      default:
        return false;
    }
  }

  renderApproveDeclineButtons(){
    return (
      <div className="form-buttons text-right">
          <Button bsStyle="danger" type="button" onClick={ () => this.handleDecline() }>Decline</Button>
          {` `}
          <Button bsStyle="success" type="button" onClick={ () => this.handleApprove() }>Approve</Button>
      </div>
    )
  }

  renderSubmitButton(){
    return (
      <div className="form-buttons text-right">
        <Button bsStyle="primary" type="submit">Submit</Button>
      </div>
    )
  }

  renderForm(){
    return (
      <Form
        onSubmit={ this.submitForm }
        ref="form"
        >
        <Input
          name="title"
          label="Title"
          value={ this.state.title }
          disabled={ this.isEditor() }
          />
        <Textarea
          rows={ 3 }
          name="body"
          label="Body"
          value={ this.state.body }
          disabled={ this.isEditor() }
        />
        <Textarea
          rows={ 3 }
          name="comment"
          label="Comment"
          value={ this.state.comment }
          disabled={ !this.isEditor() }
        />

        {
          this.isEditor()
          ? this.renderApproveDeclineButtons()
          : this.renderSubmitButton()
        }
      </Form>
    )
  }

  renderRoleSelector() {
    let roles = [
      { value: 'author', label: 'Author' },
      { value: 'copyEditor', label: 'Copy Editor' },
      { value: 'seniorEditor', label: 'Senior Editor' },
    ];

    return (
      <Form style={{ margin: '40px 0 60px 0'}}>
        <Select
          name="role"
          label="Simulate Role"
          options={roles}
          onChange={ ( _, role ) => this.handleRoleChange( role ) }
        />
      </Form>
    )
  }

  renderTable() {
    return (
      <ReactTable
        manual
        className="-highlight -striped font-small"
        data={ this.state.revisions }
        columns={ tableColumns }
        loading={ false }
        defaultPageSize={ 10 }
        showPagination={ false }
        />
    )
  }

  render() {
    let mode = this.state.title ? 'Edit' : 'Create';

    let panelHeader = (
      <span>
        <span>{mode} Article { this.state.currentRevision ? `( Revision #${ this.state.currentRevision } )`: '' }</span>
      </span>
    );

    let statusStyle = this.state.articleStatus === 'Draft' ? 'warning' : 'success';

    return (
      <div className="page-article-edit">
        { this.renderRoleSelector() }
        <PanelGroup>
          <Panel header={ panelHeader }>
            <ListGroup fill>
              <ListGroupItem>
                <Row>
                  <Col xs={6}>
                    <Label bsStyle={ statusStyle }>{ this.state.articleStatus }</Label>
                    {` `}
                    <Label>{ this.state.revisionCount } Revisions</Label>
                  </Col>
                  <Col xs={6} className="text-right">
                    { this.state.timestamp ? <span>Last Modified: <TimeAgo date={ this.state.timestamp*1000 }/></span> : [] }
                  </Col>
                </Row>
              </ListGroupItem>
              <ListGroupItem>{ this.renderForm() }</ListGroupItem>
            </ListGroup>
          </Panel>
          <Panel header="Revisions">
            <ListGroup fill>
              <ListGroupItem>{ this.renderTable() }</ListGroupItem>
            </ListGroup>
          </Panel>
        </PanelGroup>
      </div>
    );
  }
}

  let tableColumns = [
    {
      Header: 'Title',
      accessor: 'title',
      headerClassName: 'text-left',
      Cell: cell => <a href={`/article/${cell.index}`}>{cell.value}</a>
    },
    {
      Header: 'Body',
      accessor: 'body',
      headerClassName: 'text-left',
    },
    {
      Header: 'Comment',
      accessor: 'comment',
      headerClassName: 'text-left',
      Cell: cell => cell.value ? cell.value : '-',
    },
    {
      Header: 'Last Udpated',
      accessor: 'timestamp',
      headerClassName: 'text-left',
      maxWidth: 150,
      Cell: cell => <TimeAgo date={ new Date(cell.value*1000) } />
    },
  ];

export default ArticleEdit;
