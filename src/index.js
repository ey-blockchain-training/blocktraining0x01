import React from 'react';
import { render } from 'react-dom';
import { Route, BrowserRouter } from 'react-router-dom';
import Home from './pages/Home/Home';
import ArticleEdit from './pages/Article/ArticleEdit';
import ArticleList from './pages/Article/ArticleList';
import PrimaryNavigation from './components/Navigation/PrimaryNavigation';

import {
  Grid,
  Row,
  Col,
} from 'react-bootstrap';

const target = document.querySelector('#root');

render(
  <BrowserRouter>
    <Grid>
      <Row>
        <Col xs={12}>
          <PrimaryNavigation />
        </Col>
      </Row>
      <Row>
        <Col xs={12}>
          <Route path="/" exact component={ Home }/>
          <Route path="/article" exact component={ ArticleList }/>
          <Route path="/article/:id" exact component={ ArticleEdit }/>
        </Col>
      </Row>
    </Grid>
  </BrowserRouter>,
  target
);
